# Landing translator 

## Description  
This repository containes two classes: TextTranslator and LandingTranslator. TextTranslator translates text, LandingTranslator translates text in certain fields of landing.  

## TextTranslator

### Attributes 

+ ____IAM_TOKEN__ and ____folder_id__  
[Read here](https://yandex.cloud/ru/docs/translate/operations/translate)

### Methods

+ __ __init__ __(self, language, glossary)
    + __language__  
    Language is string parameter that takes languages value in format "en", "ru" and etc. that are supported by [Yandex Translate](https://yandex.cloud/ru/docs/translate/concepts/supported-languages).
    + __glossary__  
    Glossary is a dictionary of words and phrases with an unambiguous translation. 
        #### Example:
        ```
        [
            {
                "sourceText": "судья",
                "translatedText": "referee"
            },
            {
                "sourceText": "упражнение",
                "translatedText": "stage"
            }
        ]
        ```

        Lets try to translate phrase  
        `"Судью на мыло за такие упражнения"`  
        Result with empty glossary:  
        `"Judge on the soap for such exercises"`  
        And with glossary from example:  
        `"A referee on soap for such stages"`  
        [Read more here](https://yandex.cloud/ru/docs/translate/operations/better-quality)

+ __ __call__ __(self, text)
    + __text__  
    Array of strings to translate.  

    __Return__  
    Array of translated strings.

## LandingTranslator

### Attributes

+ _____units_structure__  
Structure of landing with fields to translate.

### Methods

+ __ __call__ __(self, description, language, glossary)  
    + __description__  
    The value of field __"description"__ in [Events List Instance](https://atlima.com/api/sports-events/klubnyj-match-moskovskoj-oblasti), i.e. it is the JSON of landing in string format.
    + __language__  
    Like in TextTranslator.
    + __glossary__  
    Like in TextTranslator.

    __Return__  
    JSON of translated landing in string format.

## Start  

Use next commands to install the package:  
```
git clone https://gitlab.com/117730116/landing-translator.git
cd landing-translator
```
After that you must go to src/landing_translator/translators.py and set IAM_TOKEN and id_folder attributes in class TextTranslator.  
Then run  
`pip install ./`

### Import
`from landing_translator.translators import TextTranslator, LandingTranslator`
### Example of using TextTranslator:
```
glossary = [
{
    "sourceText": "судья",
    "translatedText": "referee"
},
{
    "sourceText": "упражнение",
    "translatedText": "stage"
}
]
text_translator = TextTranslator("en", glossary)
translation = text_translator(["Судью на мыло за такие упражнения", "Меткий выстрел"])
print(translation)

# Output:
# ['A referee on soap for such stages', 'A well-aimed shot'] 
``` 

### Example of using LandingTranslator:
```
landing = "{\"units\":[{\"uuid\":\"968ba6b8-7107-493b-a08f-9d4d92762cb8\",\"text\":\"- 
действующее удостоверения ОСОО ФПСР/ IPSC;\\n- действующая спортивная страховка\",\"link_title\":\"\",\"link_url\":\"\",\"text_type\":\"standard\",\"type\":\"text\"}]}"  

json.loads(landing)

# Output:
# {'units': [{'uuid': '968ba6b8-7107-493b-a08f-9d4d92762cb8',
# 'text': '- действующее удостоверения ОСОО ФПСР/ IPSC;\n- действующая спортивная страховка',
# 'link_title': '',
# 'link_url': '',
# 'text_type': 'standard',
# 'type': 'text'}]}

landing_translator = LandingTranslator()
translated_landing = landing_translator(landing, "en", None)
json.loads(translated_landing)

# Output:
# {'units': [{'uuid': '968ba6b8-7107-493b-a08f-9d4d92762cb8',
# 'text': '- valid certificate of the LLC FPSR/ IPSC;\n- valid sports insurance',
# 'link_title': '',
# 'link_url': '',
# 'text_type': 'standard',
# 'type': 'text'}]} 
``` 
