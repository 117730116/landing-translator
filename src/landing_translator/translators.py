import json
import requests


class TextTranslator():
    """Text translator class"""
    __IAM_TOKEN = '<Your IAM_TOKEN>'
    __folder_id = '<Your folder_id>'

    def __init__(self, target_language: str, glossary: list[dict]):
        self.target_language = target_language
        self.glossary = glossary

    def __call__(self, text: list[str]) -> list[str]:
        '''
        Takes array of strings to translate, target language and glossary.\n
        Return array of translated strings.
        '''
        if not text:
            return []

        body = {
            "sourceLanguageCode": "ru",
            "targetLanguageCode": self.target_language,
            "texts": text,
            "folderId": self.__folder_id,
            "glossaryConfig": {
                "glossaryData": {
                    "glossaryPairs": self.glossary,
                }
            }
        }

        headers = {
            "Content-Type": "application/json",
            "Authorization": "Api-Key {0}".format(self.__IAM_TOKEN)
        }

        response = json.loads(
            requests.post('https://translate.api.cloud.yandex.net/translate/v2/translate',
                json = body,
                headers = headers).text
            )

        return [translation["text"] for translation in response["translations"]]


class LandingTranslator():
    """Translator of landing class"""
    __units_structure = {
        "type": {
            "text": {
                "subtype": "text_type",
                "fields": {
                    "standard": {
                        "subfield": "",
                        "fields": ["text"],
                    },
                    "bold": {
                        "subfield": "",
                        "fields": ["text"],
                    },
                    "num_list": {
                        "subfield": "",
                        "fields": ["text"],
                    },
                    "bull_list": {
                        "subfield": "",
                        "fields": ["text"],
                    },
                    "link": {
                        "subfield": "",
                        "fields": ["link_title"],
                    },
                }
            },
            "paragraph": {
                "subtype": "",
                "fields": {
                    "": {
                        "subfield": "",
                        "fields": ["title", "description"]
                    }
                }
            },
            "divider": {
                "subtype": "div_type",
                "fields": {
                    "title": {
                        "subfield": "",
                        "fields": ["text"],
                    },
                    "image": {
                        "subfield": "",
                        "fields": ["text"],
                    }
                }
            },
            "imageWithText": {
                "subtype": "",
                "fields": {
                    "": {
                        "subfield": "",
                        "fields": ["text"]
                    }
                }
            },
            "document": {
                "subtype": "",
                "fields": {
                    "": {
                        "subfield": "doc",
                        "fields": ["title", "filename"]
                    }
                }
            }
        }
    }

    def __call__(self, description: str, language: str, glossary: list[dict]) -> str:
        '''
        Takes description block in string type, target language and glossary.\n
        Return translated description block in string type.
        '''
        if not description:
            return ""

        translator = TextTranslator(language, glossary)
        landing = json.loads(description)
        units = landing["units"]
        for i in range(len(units)):
            unit_type = units[i]["type"]
            unit_subtype = self.__units_structure["type"][unit_type]["subtype"]
            unit_subtype_value = units[i][unit_subtype] if unit_subtype else ""
            unit_subfield = self.__units_structure["type"][unit_type]["fields"][unit_subtype_value]["subfield"]
            for field in self.__units_structure["type"][unit_type]["fields"][unit_subtype_value]["fields"]:
                text = units[i][unit_subfield][field] if unit_subfield else units[i][field]
                if text:
                    text = text.split('\n')
                    translated_text = '\n'.join(translator(text))
                    if unit_subfield:
                        units[i][unit_subfield][field] = translated_text
                    else:
                        units[i][field] = translated_text
                else:
                    continue
        
        landing["units"] = units
        return json.dumps(landing)
